FROM pandoc/core:2.14.1 as pandoc-binaries

FROM asciidoctor/docker-asciidoctor:1.11.0

LABEL maintainer="Frédéric Gracia <fgracia@sqli.com>"
LABEL maintainer="Eric Léon <eleon@sqli.com>"

# Ajout pandoc
COPY --from=pandoc-binaries /usr/local/bin/pandoc* /usr/bin/
RUN apk add --no-cache \
         gmp \
         libffi \
         lua5.3 \
         lua5.3-lpeg

# On configure la timezone pour éviter le décalage horaire
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Ajout du script run.sh
ADD run.sh /run.sh
RUN chmod +x /run.sh

# Copie des ressources
ADD src/asciidoctor-extensions-lab /opt/asciidoctor-extensions-lab
ADD src/asciidoctor-chart /opt/asciidoctor-chart
ADD src/templatesqli /opt/templatesqli

ENV SOURCE_DIRECTORY /documents
ENV TARGET_DIRECTORY /documents

# Run
ENTRYPOINT ["/run.sh"]