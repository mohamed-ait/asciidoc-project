#!/bin/bash

# Constantes
VERT="\\033[1;32m"
NORMAL="\\033[0;39m"
ROUGE="\\033[1;31m"
JAUNE="\\033[1;33m"

# Lien vers les sources des différentes dépendances pour asciidoctor
REVEAL_JS_GIT_URL='https://github.com/hakimel/reveal.js.git'
# Releases disponibles : https://github.com/hakimel/reveal.js/releases
# Issue avec les versions > 4.1.0 (https://github.com/hakimel/reveal.js/issues/2978), empêche le fond de la page de garde de s'afficher
REVEAL_JS_VERSION='4.1.0'
REVEAL_JS_PLUGINS_GIT_URL='https://github.com/rajgoel/reveal.js-plugins'
ASCIIDOCTOR_REVEAL_JS_GIT_URL='git://github.com/asciidoctor/asciidoctor-reveal.js.git'
ASCIIDOCTOR_EXT_LAB_GIT_URL='https://github.com/asciidoctor/asciidoctor-extensions-lab.git'
ASCIIDOCTOR_STYLE_FACTORY_GIT_URL='https://github.com/asciidoctor/asciidoctor-stylesheet-factory.git'
ASCIIDOCTOR_CHART="https://github.com/asciidoctor/asciidoctor-chart.git"
ASCIIDOCTOR_DOCKER_IMAGE="registry-private.docker.iscbordeaux.lan.bdx.sqli.com/sqli/asciidoctor"

COMPASS_DOCKER_IMAGE='registry-private.docker.iscbordeaux.lan.bdx.sqli.com/rte/compass:latest'
GRUNT_DOCKER_IMAGE='registry-private.docker.iscbordeaux.lan.bdx.sqli.com/sqli/node-grunt:15.5.1'

logger() {
  case ${1} in
    ERROR)
      echo -e "$ROUGE""[ERROR] "$2"$NORMAL"
      ;;
    ALERT)
      echo -e "$JAUNE""[ALERT] "$2"$NORMAL"
      ;;
    INFO)
      echo -e "$VERT""[INFO] "$2"$NORMAL"
      ;;
  esac
}

install_dep() {
  logger INFO "Installation des librairies"
  if [[ ! -d src/reveal.js ]] ; then
    logger INFO "Installation du dépôt reveal.js"
    git clone $REVEAL_JS_GIT_URL --branch $REVEAL_JS_VERSION src/reveal.js
    docker run --rm -ti -v $(pwd)/src/reveal.js:/data ${GRUNT_DOCKER_IMAGE} npm install
    sudo chown -R ${USER}:${USER} ./src/reveal.js/node_modules
  fi
  if [[ ! -d src/reveal.js-plugins ]] ; then
    logger INFO "Installation du dépôt reveal.js-plugins"
    git clone $REVEAL_JS_PLUGINS_GIT_URL src/reveal.js-plugins
    cp -rf $(pwd)/src/reveal.js-plugins/chart $(pwd)/src/reveal.js/plugin/
  fi
  if [[ ! -d src/asciidoctor-reveal.js ]] ; then
    logger INFO "Installation du dépôt asciidoctor-reveal.js"
    git clone $ASCIIDOCTOR_REVEAL_JS_GIT_URL src/asciidoctor-reveal.js
  fi
  if [[ ! -d src/asciidoctor-stylesheet-factory ]] ; then
    logger INFO "Installation du dépôt asciidoctor-stylesheet-factory"
    git clone $ASCIIDOCTOR_STYLE_FACTORY_GIT_URL src/asciidoctor-stylesheet-factory
  fi
  if [[ ! -d src/asciidoctor-extensions-lab ]] ; then
    logger INFO "Installation du dépôt asciidoctor-extensions-lab"
    git clone $ASCIIDOCTOR_EXT_LAB_GIT_URL src/asciidoctor-extensions-lab
  fi
  if [[ ! -d src/asciidoctor-chart ]] ; then
    logger INFO "Installation du dépôt asciidoctor-chart"
    git clone $ASCIIDOCTOR_CHART src/asciidoctor-chart
  fi
}

clean_install() {
  logger INFO "Suppression du dossier ./src/reveal.js*"
  rm -rf ./src/reveal.js*
  logger INFO "Suppression du dossier ./src/asciidoctor-*"
  rm -rf ./src/asciidoctor-*
  logger INFO "Suppression du dossier rm -rf ./templatesqli"
  rm -rf ./src/templatesqli
}

# Compilation des templates html et copie dans le répertoire de travail
make_html_templates() {
  # Si le dossier de destination n'existe pas ou est vide, on crée l'arborescence
  if [[ ! -d $(pwd)/src/templatesqli/HTML5 ]] || [[ ! "$(ls -A $(pwd)/src/templatesqli/HTML5)" ]] ; then
    mkdir -p $(pwd)/src/templatesqli/HTML5
    mkdir -p $(pwd)/src/templatesqli/HTML5/images
    mkdir -p $(pwd)/src/templatesqli/HTML5/images/sqlitemplate
  fi
  # Compilation des templates
  logger INFO "Compilation des templates HTML"
  cp -r $(pwd)/src/resources/HTML5/sass/* $(pwd)/src/asciidoctor-stylesheet-factory/sass
  cp -r $(pwd)/src/resources/fonts/* $(pwd)/src/asciidoctor-stylesheet-factory/sass/fonts
  docker run --rm -ti -v $(pwd)/src/asciidoctor-stylesheet-factory/:/input -v $(pwd)/src/templatesqli/HTML5/stylesheets:/output $COMPASS_DOCKER_IMAGE compile
  # Copie des ressources dans le répertoire de destination
  cp -r $(pwd)/src/asciidoctor-stylesheet-factory/images/* $(pwd)/src/templatesqli/HTML5/images
  cp -r $(pwd)/src/asciidoctor-stylesheet-factory/stylesheets/* $(pwd)/src/templatesqli/HTML5/stylesheets
  cp -r $(pwd)/src/resources/images/* $(pwd)/src/templatesqli/HTML5/images/sqlitemplate
  cp -r $(pwd)/src/resources/fonts $(pwd)/src/templatesqli/HTML5
}

make_slide_templates() {
  # On vérifie que les packages node soient présents
  if [[ ! -d $(pwd)/src/reveal.js/node_modules ]] || [[ ! "$(ls -A $(pwd)/src/reveal.js/node_modules)" ]] ; then
    logger ALERT "Modules node manquants."
    install_dep
  fi
  # Création de l'arborescence
  if [[ ! -d $(pwd)/src/templatesqli/SLIDE ]] ; then
    mkdir -p $(pwd)/src/templatesqli/SLIDE
    mkdir -p $(pwd)/src/templatesqli/SLIDE/slim
    mkdir -p $(pwd)/src/templatesqli/SLIDE/reveal.js
    mkdir -p $(pwd)/src/templatesqli/SLIDE/reveal.js/js
    mkdir -p $(pwd)/src/templatesqli/SLIDE/reveal.js/css
    mkdir -p $(pwd)/src/templatesqli/SLIDE/reveal.js/css/print
  fi
  logger INFO "Compilation des templates reveal.js"
  cp $(pwd)/src/resources/SLIDE/sass/* $(pwd)/src/reveal.js/css/theme/source
  docker run --rm -ti -v $(pwd)/src/reveal.js:/data $GRUNT_DOCKER_IMAGE npm run build -- css-themes
  # Copie des templates reveal.js dans le dossier de destination
  cp -r $(pwd)/src/reveal.js/dist $(pwd)/src/templatesqli/SLIDE/reveal.js/
  cp -r $(pwd)/src/resources/images $(pwd)/src/templatesqli/SLIDE/reveal.js/dist/theme
  cp -r $(pwd)/src/resources/fonts $(pwd)/src/templatesqli/SLIDE/reveal.js/dist/theme
  cp -r $(pwd)/src/reveal.js/dist/reveal.js $(pwd)/src/templatesqli/SLIDE/reveal.js/js
  cp -r $(pwd)/src/reveal.js/dist/*.css $(pwd)/src/templatesqli/SLIDE/reveal.js/css
  cp -r $(pwd)/src/reveal.js/dist/theme $(pwd)/src/templatesqli/SLIDE/reveal.js/css
  cp -r $(pwd)/src/reveal.js/dist/theme/paper.css $(pwd)/src/templatesqli/SLIDE/reveal.js/css/print/
  cp -r $(pwd)/src/resources/images $(pwd)/src/templatesqli/SLIDE/reveal.js/css/theme
  cp -r $(pwd)/src/resources/fonts $(pwd)/src/templatesqli/SLIDE/reveal.js/css/theme
  cp -r $(pwd)/src/reveal.js/plugin $(pwd)/src/templatesqli/SLIDE/reveal.js
  cp -r $(pwd)/src/asciidoctor-reveal.js/templates/* $(pwd)/src/templatesqli/SLIDE/slim
  cp -r $(pwd)/src/resources/SLIDE/revealjs_plugins $(pwd)/src/templatesqli/SLIDE/
}

make_pdf_templates() {
  # Création de l'arborescence
  if [[ ! -d $(pwd)/src/templatesqli/PDF ]] ; then
    mkdir $(pwd)/src/templatesqli/PDF
  fi
  cp -r $(pwd)/src/resources/PDF $(pwd)/src/templatesqli
  cp -r $(pwd)/src/resources/fonts/* $(pwd)/src/templatesqli/PDF/data/fonts
  cp -r $(pwd)/src/resources/images/* $(pwd)/src/templatesqli/PDF/data/images
}

make_pptx_templates() {
  # Création de l'arborescence
  if [[ ! -d $(pwd)/src/templatesqli/PPTX ]] ; then
    mkdir -p $(pwd)/src/templatesqli/PPTX
  fi
  cp -r $(pwd)/src/resources/PPTX $(pwd)/src/templatesqli
}

make_docx_templates() {
  # Création de l'arborescence
  if [[ ! -d $(pwd)/src/templatesqli/DOCX ]] ; then
    mkdir -p $(pwd)/src/templatesqli/DOCX
  fi
  cp -r $(pwd)/src/resources/DOCX $(pwd)/src/templatesqli
}

# Construction de l'image
build_docker_image(){
  logger INFO "###########"
  logger INFO "#  Build  #"
  logger INFO "###########"

  # Check if src are ok
  logger INFO "Génération des templates avant construction de l'image"
  if [[ ! -d $(pwd)/src/templatesqli/HTML5 ]] ; then
    make_html_templates
  fi

  if [[ ! -d $(pwd)/src/templatesqli/SLIDE ]] ; then
    make_slide_templates
  fi

  if [[ ! -d $(pwd)/src/templatesqli/PDF ]] ; then
    make_pdf_templates
  fi

  if [[ ! -d $(pwd)/src/templatesqli/PPTX ]] ; then
    make_pptx_templates
  fi

 if [[ ! -d $(pwd)/src/templatesqli/DOCX ]] ; then
    make_docx_templates
  fi

  logger INFO "Construction de l'image ${ASCIIDOCTOR_DOCKER_IMAGE}"
  docker build -t ${ASCIIDOCTOR_DOCKER_IMAGE} .
  exit 0
}

display_help(){
  logger INFO "asciidoctor builder usage"
  printf "$VERT""---------------------------------\n\n"
  printf "$VERT""usage : /build.sh <option>""$NORMAL""\n\n"
  printf "example : ""$JAUNE""./build.sh --slide""$NORMAL"" make produce all needs in order to generate revealjs slide with asciidoctor.\n\n"
  echo "--build-image | -b : build docker image for registry-private.docker.iscbordeaux.lan.bdx.sqli.com/sqli/asciidoctor and resources"
  echo "--install | -i : install packages for html templates compilation"
  echo "--clean : removes local folders that are populated by --install option"
  echo "--html : generate html templates ressources for the docker image"
  echo "--slide | -s : generate revealjs slide template for the docker image"
  echo "--dev <html|pdf|slide|pptx|docx> <theme> <path/to/file.adoc> : cette option permet de générer rapidement un document dans le format et le theme choisi avec les ressources locales."
  echo "--help | -h : display this help"
}

make_slide() {
  fileName=${1}

  if [[ ${2} == "" ]]; then
    logger ALERT "Pas de thème spécifié. Choix du thème par défaut : sqlislide"
    theme="sqlislide"
  else
    theme=${2}
  fi

  if [[ ! fileName == "" ]]; then
    make_slide_templates
    docker run --rm -v $(pwd)/src/templatesqli:/templatesqli -v $(pwd):/documents -e "ENV=dev" -e "FORMAT=revealjs" -e "THEME=${theme}" -e "CSS=offline" -e "IMAGES=in-local" ${ASCIIDOCTOR_DOCKER_IMAGE} ${fileName}
    exit 1;
  fi
}

make_html() {
  fileName=${1}

  if [[ ${2} == "" ]]; then
    logger ALERT "Pas de thème spécifié. Choix du thème par défaut : sqli"
    theme="sqli"
  else
    theme=${2}
  fi

  if [[ ! fileName == "" ]]; then
    make_slide_templates
    docker run --rm -v $(pwd)/src/templatesqli:/templatesqli -v $(pwd):/documents -e "ENV=dev" -e "FORMAT=html" -e "THEME=${theme}" -e "CSS=offline" -e "IMAGES=in-local" ${ASCIIDOCTOR_DOCKER_IMAGE} ${fileName}
    exit 1;
  fi
}

make_html-sqli() {
  fileName=${1}

  if [[ ! fileName == "" ]]; then
    make_slide_templates
    docker run --rm -v $(pwd)/src/templatesqli:/templatesqli -v $(pwd):/documents -e "ENV=dev" -e "FORMAT=html-sqli" -e "CSS=online" -e "IMAGES=in-local" ${ASCIIDOCTOR_DOCKER_IMAGE} ${fileName}
    exit 1;
  fi
}

make_pdf() {
  fileName=${1}

  if [[ ${2} == "" ]]; then
    logger ALERT "Pas de thème spécifié. Choix du thème par défaut : sqli"
    theme="sqli"
  else
    theme=${2}
  fi

  if [[ ! fileName == "" ]]; then
    docker run --rm -v $(pwd)/src/templatesqli:/templatesqli -v $(pwd):/documents -e "ENV=dev" -e "FORMAT=pdf" -e "THEME=${theme}" ${ASCIIDOCTOR_DOCKER_IMAGE} ${fileName}
    exit 1;
  fi
}

make_pptx() {
  fileName=${1}

  if [[ ${2} == "" ]]; then
    logger ALERT "Pas de thème spécifié. Choix du thème par défaut : sqli"
    theme="sqli"
  else
    theme=${2}
  fi

  if [[ ! fileName == "" ]]; then
    docker run --rm -v $(pwd)/src/templatesqli:/templatesqli -v $(pwd):/documents -e "ENV=dev" -e "FORMAT=pptx" -e "THEME=${theme}" ${ASCIIDOCTOR_DOCKER_IMAGE} ${fileName}
    exit 1;
  fi
}

make_docx() {
  fileName=${1}

  if [[ ${2} == "" ]]; then
    logger ALERT "Pas de thème spécifié. Choix du thème par défaut : sqli"
    theme="sqli"
  else
    theme=${2}
  fi

  if [[ ! fileName == "" ]]; then
    docker run --rm -v $(pwd)/src/templatesqli:/templatesqli -v $(pwd):/documents -e "ENV=dev" -e "FORMAT=docx" -e "THEME=${theme}" ${ASCIIDOCTOR_DOCKER_IMAGE} ${fileName}
    exit 1;
  fi
}

OPTION_SELECTED="NONE"

echo ""
echo -e "$VERT""                    _ _     _            _             "
echo -e "$VERT""     /\            (_|_)   | |          | |            "
echo -e "$VERT""    /  \   ___  ___ _ _  __| | ___   ___| |_ ___  _ __ "
echo -e "$VERT""   / /\ \ / __|/ __| | |/ _\` |/ _ \ / __| __/ _ \| '__|"
echo -e "$VERT""  / ____ \ __ \ (__| | | (_| | (_) | (__| || (_) | |   "
echo -e "$VERT"" /_/    \_\___/\___|_|_|\__,_|\___/ \___|\__\___/|_|   "
echo ""

while [ ! "$1" == "" ]
do
  i=${1}
  case ${i} in
    --install|-i)
      # Installation des dépendances
      install_dep
    ;;

    --clean)
      # Nettoyage de l'installation
      clean_install
    ;;

    --build-image|-b)
      # Construction de l'image docker
      build_docker_image
    ;;

    --html)
      # Compilation des thèmes html
      make_html_templates
    ;;

    --slide|-s)
      # Compilation des thèmes pour les slide reveal.js
      make_slide_templates
    ;;

    --help|-h)
      # Affichage de l'aide
      display_help
    ;;

    --dev|-d)
      if [[ ! ${3} == "" ]]; then
        case ${2} in
          html)
            make_html ${3}
            ;;
          html-sqli)
            make_html-sqli ${3}
            ;;
          slide)
            make_slide ${3}
            ;;
          pdf)
            make_pdf ${3}
            ;;
          pptx)
            make_pptx ${3}
            ;;
          docx)
            make_docx ${3}
            ;;
          *)
            logger ERROR "Argument ${2} non valide"
            exit 1
            ;;
        esac
        logger INFO "Génération du fichier ${3} en version ${2} avec les sources de dev."
      else
        logger ERROR "Il faut spécifier un nom de fichier."
        exit 1
      fi
      ;;

    *)
      logger ERROR "L'argument ${i} n'est pas valide"
      exit 1;
      ;;
  esac
  OPTION_SELECTED=${1}
  shift
done

if [[ "$OPTION_SELECTED" == "NONE" ]]; then
  logger ERROR "option missed"
	printf "usage ${0} <option> \nSee README.adoc located at https://gitlab.bordeaux.sqli.com/docker/asciidoctor/tree/master"
	printf "\nor type ./build.sh -h to display help\n"
	exit;
fi
