= Menu contextuel Ubuntu : génération Asciidoc
:sectnums:
:author: Frédéric GRACIA
:email: fgracia@sqli.com
:revdate: 03/02/2020

Il est possible sur Ubuntu de générer à partir d'un fichier Asciidoc des présentations aux formats HTML, PDF, slides, DOCX ou encore PPTX d'un simple clic droit :

image::images/nautilus_menu_contextuel_asciidoc_1.png[]

<<<<

image::images/nautilus_menu_contextuel_asciidoc_2.png[]


== Prérequis

. http://presentations.docker.iscbordeaux.lan.bdx.sqli.com/crci/HTML_SQLI_00-wiki-crci.html#_installer_docker_et_docker_compose[Installer Docker (déjà installé sur les postes SQLI Ubuntu)]
. http://presentations.docker.iscbordeaux.lan.bdx.sqli.com/crci/HTML_SQLI_00-wiki-crci.html#_modifier_les_options_par_défaut_du_démon_docker[Configurer le démon Docker]



== Installation

NOTE: *Cette fonctionnalité est déjà installée par défaut sur tous les postes Linux d'ISC Bordeaux*

Cloner ce repo Git et exécuter le script *install.sh* :

[source, bash]
----
git clone ssh://git@gitlab.lan.bdx.sqli.com:10022/docker/asciidoctor.git
cd asciidoctor/integration-nautilus-ubuntu
sh install.sh
----

Ouvrez un Terminal et tapez les commandes suivantes pour recharger nautilus (ferme tous les explorateurs de fichiers ouverts):
[source, bash]
----
nautilus -q && nautilus &
----

*OU*

Fermez votre session puis reconnectez-vous.

Vous pouvez désormais faire un clic droit sur un fichier *.adoc* et procéder à la génération désirée.


== Mettre à jour l'outil

Si l'outil de génération est mis à jour sur Gitlab (uniquement la fonctionnalité par clic droit),
il pourra être mis à jour sur votre poste en passant par le clic droit lui-même >
*FileManager-Actions actions* > *Update Asciidoc generation tool*


== Fonctionnalité cachée

Lorque vous avez besoin de générer régulièrement une documentation sur votre poste,
ce n'est pas pratique d'utiliser systématiquement la génération par clic droit,
et il n'est pas simple non plus de retenir tous les paramètres de la ligne de commandes.

Afin de résoudre ce problème, à la fin de chaque génération par clic droit,
la commande complète de génération est injectée dans votre presse-papier.

Afin de profiter de cette fonctionnalité :

. Assurez-vous d'avoir la dernière version de l'outil (cf. chapitre précédent)
. Installez le paquet _xclip_ (installé par défaut sur tous les postes Linux ISC Bordeaux):

[source, bash]
----
sudo apt-get install xclip
----
