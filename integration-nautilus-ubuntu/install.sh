#!/bin/bash

# Ce script installe les outils nécessaires pour la génération Asciidoc par clic droit sur Ubuntu
# Ce script peut être exécuté de 2 manières différentes :
#   - par le user root, en précisant en paramètre le user concerné
#   - par le user concerné, sans paramètre

UTILISATEUR=$USER
FILEMANAGER_ACTIONS_VERSION="3.4-1"
USER_GROUP=$(id -gn ${UTILISATEUR})

# Si le script est exécuté par root, mais qu'il n'y a pas de paramètre, on sort du script
if [ `whoami` = root ] && [ $# -ne 1 ]
then
  echo "[ERREUR] Exécuté par root, ce script doit prendre en paramètre le user final."
  exit 1
fi

# Si le script est exécuté par root et qu'un paramètre est donné, on sette l'utilisateur donné
if [ `whoami` = root ] && [ $# -eq 1 ]
then
  UTILISATEUR=$1
fi

echo "[INFO] L'installation sera faite pour l'utilisateur ${UTILISATEUR}"

# Fonction pour vérifier l'état de la commande précédente
check() {
  if [ $? = 0 ]
    then
      echo -e "\nInstallation OK, pour prendre en compte les modifications vous pouvez soit :"
      echo -e "\n=> Ouvrez un Terminal et tapez la commande suivante : nautilus -q && nautilus &"
      echo -e "\n=> Sinon, fermez votre session et reconnectez-vous pour que ce soit effectif."
  else
      echo -e "\nErreur lors de l'installation."
      exit 1
  fi
}

echo "[INFO] Installation en cours du clic droit to asciidoc..."

# Le paquet nautilus-actions n'existant plus sur Ubuntu 18.04, on installe différemment

if [ `lsb_release -r -s` = 18.04 ] ; then
  wget --quiet "http://binaires.docker.iscbordeaux.lan.bdx.sqli.com/filemanager-actions/${FILEMANAGER_ACTIONS_VERSION}/filemanager-actions_${FILEMANAGER_ACTIONS_VERSION}_amd64.deb" \
  && sudo apt-get install -y ./filemanager-actions_${FILEMANAGER_ACTIONS_VERSION}_amd64.deb 1> /dev/null

else
  sudo apt-get install -y nautilus-actions 1> /dev/null
fi


if [ -d /usr/local/scripts/nautilus-actions ] ; then
  echo "  - Nettoyage du dossier /usr/local/scripts/nautilus-actions..."
  sudo rm -rf /usr/local/scripts/nautilus-actions
fi

if [ -d /home/${UTILISATEUR}/.local/share/file-manager/actions ] ; then
  echo "  - Nettoyage du dossier /home/${UTILISATEUR}/.local/share/file-manager/actions..."
  rm -rf /home/${UTILISATEUR}/.local/share/file-manager/actions
fi


echo "  - Copie en local du script de génération et du menu contextuel..."
if [ ! -d /usr/local/scripts/nautilus-actions/integration-nautilus-ubuntu ] ; then
  sudo mkdir -p /usr/local/scripts/nautilus-actions/integration-nautilus-ubuntu
fi


mkdir -p /home/${UTILISATEUR}/.local/share/file-manager/actions
cp ./nautilus* /home/${UTILISATEUR}/.local/share/file-manager/actions
sudo cp ./asciidoc_generate_files.sh /usr/local/scripts/nautilus-actions/integration-nautilus-ubuntu \
&& sudo cp ./asciidoc_update_tool.sh /usr/local/scripts/nautilus-actions/integration-nautilus-ubuntu
check

echo "  - Application des droits de l'utilisateur ${UTILISATEUR} sur le dossier /usr/local/scripts/nautilus-actions"
sudo chown -R ${UTILISATEUR}:"${USER_GROUP}" /usr/local/scripts/nautilus-actions

echo "  - Application des droits de l'utilisateur ${UTILISATEUR} sur le dossier /home/${UTILISATEUR}/.local/share"
sudo chown -R ${UTILISATEUR}:"${USER_GROUP}" /home/${UTILISATEUR}/.local/share
