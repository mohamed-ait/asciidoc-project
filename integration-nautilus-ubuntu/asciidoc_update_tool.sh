#!/bin/bash

# Variables
LOG_FILE="/tmp/asciidoc_update.log"
TEMPORARY_REPO_FOLDER="/tmp/docker-asciidoctor"


# Cleaning log file
if [ -f $LOG_FILE ] ; then
  rm -f $LOG_FILE
fi


# Cleaning Git folder
if [ -d $TEMPORARY_REPO_FOLDER ] ; then
  rm -rf $TEMPORARY_REPO_FOLDER
fi


# Testing Gitlab access
if ping -c 1 gitlab.lan.bdx.sqli.com 1> /dev/null ; then
  echo "### Git clone..." >> $LOG_FILE
  git clone ssh://git@gitlab.lan.bdx.sqli.com:10022/docker/asciidoctor.git $TEMPORARY_REPO_FOLDER 2>> $LOG_FILE \
  && echo "\n### Copying nautilus files..." >> $LOG_FILE \
  && cp $TEMPORARY_REPO_FOLDER/integration-nautilus-ubuntu/nautilus* ~/.local/share/file-manager/actions 2>> $LOG_FILE \
  && echo "\n### Copying scripts..." >> $LOG_FILE \
  && cp $TEMPORARY_REPO_FOLDER/integration-nautilus-ubuntu/asciidoc_generate_files.sh /usr/local/scripts/nautilus-actions/integration-nautilus-ubuntu 2>> $LOG_FILE \
  && cp $TEMPORARY_REPO_FOLDER/integration-nautilus-ubuntu/asciidoc_update_tool.sh /usr/local/scripts/nautilus-actions/integration-nautilus-ubuntu 2>> $LOG_FILE

  if [ $? != 0 ] ; then
    ERROR=`cat $LOG_FILE`
    zenity --error \
        --text="$ERROR \
        \n\nYou may get more info by running this script manually : $TEMPORARY_REPO_FOLDER/integration-nautilus-ubuntu/asciidoc_update_tool.sh"

  else
    notify-send "Asciidoctor tool updated" --icon=dialog-information
    rm -rf $TEMPORARY_REPO_FOLDER
  fi


else
  zenity --error \
      --text="Gitlab is not available. Please check your network."
  exit
fi
