#!/bin/bash
# Ce script affiche une popup permettant de générer des documents à partir d'Asciidoc
# Il utilise l'image Docker Asciidoctor SQLI


########################
## Début des variables

DIRECTORY=${1}
FILE=${2}

LOG_DOCKER_PULL="/tmp/asciidoc_docker_pull_error.log"
LOG_ASCIIDOC_GENERATE="/tmp/asciidoc_generate.log"
LOG_ASCIIDOC_GENERATE_ERROR="/tmp/asciidoc_generate_error.log"
CSS_URL_TO_DISPLAY=https://asciidoc-templates.bordeaux.sqli.com/
ASCIIDOCTOR_DOCKER_IMAGE="registry-private.docker.iscbordeaux.lan.bdx.sqli.com/sqli/asciidoctor:latest"

## Fin des variables
########################


########################
## Début des fonctions

clean_logs() {
  if [ -f "${LOG_DOCKER_PULL}" ] ; then
    rm -f ${LOG_DOCKER_PULL}
  fi

  if [ -f "${LOG_ASCIIDOC_GENERATE}" ] ; then
    rm -f ${LOG_ASCIIDOC_GENERATE}
  fi

  if [ -f "${LOG_ASCIIDOC_GENERATE_ERROR}" ] ; then
    rm -f ${LOG_ASCIIDOC_GENERATE_ERROR}
  fi
}

clean_containers() {
  docker ps -a | grep "converthtmltopdf"
  if [ $? = 0 ] ; then
    docker rm -f converthtmltopdf
  fi

  docker ps -a | grep "convert"
  if [ $? = 0 ] ; then
    docker rm -f convert
  fi
}

check_if_cancel_button() {
  if [ "$?" = 1 ] ; then
#    zenity --error \
#      --text="Job canceled."
      clean_containers
      exit 1
  fi
}

question_embedded_css() {
zenity --question \
--title "Generate Asciidoc : embedded CSS" \
--text "Copy on your local computer CSS files (works offline)? \
\nIf not, this URL will be used to load CSS (works <b>online only</b>) : ${CSS_URL_TO_DISPLAY}."

# On récupère la réponse pour savoir s'il faudra ou pas copier en local les templates
if [ $? = 0 ] ; then
  EMBEDDED_CSS="offline"
else
  EMBEDDED_CSS="online"
fi

check_if_cancel_button
}


question_revealjs_theme() {
  # Fenêtre de choix de thème reveal.js
  REVEALJSTHEME=$(zenity --entry --entry-text="league" --title="Asciidoc generation : reveal.js theme" \
  --text="Choose a reveal.js theme :" \
  "black" \
  "white" \
  "sky" \
  "beige" \
  "simple" \
  "serif" \
  "blood" \
  "night" \
  "moon" \
  "solarized");

  check_if_cancel_button

}

question_html_to_pdf() {
zenity --question \
  --title "Convert HTML slides to PDF" \
  --text "Convert HTML slides to PDF?"

  # On récupère la réponse pour savoir s'il faudra ou pas générer les slides en PDF
  if [ $? = 0 ] ; then
    CONVERT_REVEALJS_TO_PDF=${1}
  fi

  check_if_cancel_button

}


## Fin des fonctions
########################

########################
## Début du script

# Nettoyage des logs existants
clean_logs

# Nettoyage des containers Docker
clean_containers

# Fenêtre de choix de format de sortie
FORMAT=$(zenity --entry --title="Asciidoc generation : output format" \
--text="Choose an output format :" \
"HTML" \
"HTML Template SQLI" \
"PDF" \
"PDF Template SQLI" \
"PDF Template RTE" \
"Slides Reveal.js" \
"Slides Reveal.js Template SQLI" \
"DOCX Template SQLI" \
"PPTX Template SQLI");

check_if_cancel_button


if [ "${FORMAT}" = "HTML" ] ; then
  FORMAT=html
fi

if [ "${FORMAT}" = "HTML Template SQLI" ] ; then
  FORMAT=html-sqli
  question_embedded_css
fi

if [ "${FORMAT}" = "PDF" ] ; then
  FORMAT=pdf
fi

if [ "${FORMAT}" = "PDF Template SQLI" ] ; then
  FORMAT=pdf
  THEME=sqli
fi

if [ "${FORMAT}" = "PDF Template RTE" ] ; then
  FORMAT=pdf
  THEME=rte
fi

if [ "${FORMAT}" = "Slides Reveal.js" ] ; then
  FORMAT=revealjs
  question_embedded_css
  question_revealjs_theme
  question_html_to_pdf revealjs
fi

if [ "${FORMAT}" = "Slides Reveal.js Template SQLI" ] ; then
  FORMAT=revealjs-sqli
  question_embedded_css
  question_html_to_pdf revealjs-sqli
fi

if [ "${FORMAT}" = "DOCX Template SQLI" ] ; then
  FORMAT=docx
  THEME=sqli
fi

if [ "${FORMAT}" = "PPTX Template SQLI" ] ; then
  FORMAT=pptx
  THEME=sqli
fi

### Début de la barre de progression
(
echo "30"
echo "Checking if private docker registry is reachable..."
ping -c 1 registry-private.docker.iscbordeaux.lan.bdx.sqli.com 1> /dev/null

if [ $? = 0 ] ; then
  echo "# Pulling latest version of Asciidoctor Docker image..."
  docker pull ${ASCIIDOCTOR_DOCKER_IMAGE} 2> ${LOG_DOCKER_PULL}
else
  echo "WARNING : docker registry is unreachable, current local docker image will be used"
fi


# On affiche l'erreur du pull s'il y en a eu
ERROR_PULL=$(cat ${LOG_DOCKER_PULL})
if [ "${ERROR_PULL}" != "" ] ; then
  zenity --error \
  --text="${ERROR_PULL}"
fi


echo "60"
echo "# Generating document..."

docker run --name convert --rm -e FORMAT="${FORMAT}" \
-e REVEALJSTHEME="${REVEALJSTHEME}" \
-e CSS="${EMBEDDED_CSS}" \
-e THEME=${THEME} \
-v "${DIRECTORY}":/documents \
-v /etc/localtime:/etc/localtime:ro \
${ASCIIDOCTOR_DOCKER_IMAGE} "${FILE}" \
1> ${LOG_ASCIIDOC_GENERATE} \
2> ${LOG_ASCIIDOC_GENERATE_ERROR}

ERROR=$(cat ${LOG_ASCIIDOC_GENERATE_ERROR})

COMMAND="docker run --name convert --rm -e FORMAT="${FORMAT}" \
	-e REVEALJSTHEME="${REVEALJSTHEME}" \
	-e CSS="${EMBEDDED_CSS}" \
	-e THEME=${THEME} \
	-v "${DIRECTORY}":/documents \
	-v /etc/localtime:/etc/localtime:ro \
	${ASCIIDOCTOR_DOCKER_IMAGE} "${FILE}""

# Si le paquet xclip est installé (Ubuntu), on copie la commande docker run précédente dans le presse-papier
# Il existe 3 presse papier différents, d'où les 3 xclip
which xclip 1> /dev/null

if [ $? = 0 ] ; then
  echo ${COMMAND} | xclip -silent -selection primary
  echo ${COMMAND} | xclip -silent -selection secondary
  echo ${COMMAND} | xclip -silent -selection clipboard
  # xclip lance des process en tâche de fond, il faut les killer pour que la barre de progression zenity rende la main
  # Recommenté car la copie dans le presse-papier n'est sinon plus faite
  #killall xclip
fi

# Si le paquet pbcopy est installé (MacOS), on copie la commande docker run précédente dans le presse-papier
which pbcopy 1> /dev/null

if [ $? = 0 ] ; then
  echo ${COMMAND} | pbcopy
fi


# S'il y a eu une erreur lors de la génération, on l'affiche par une popup centrale
if [ "${ERROR}" != "" ] ; then
zenity --error \
    --text="${ERROR}"
fi


if [ "${CONVERT_REVEALJS_TO_PDF}" = "revealjs" ] ; then
  echo "80"
  echo "# Converting HTML to PDF. It may take a while..."
  docker run --name converthtmltopdf --rm --user ${UID}:${UID} -v "${DIRECTORY}":/slides astefanutti/decktape:1.0.0 \
  SLIDE_REVEALJS_"${FILE%.*}".html SLIDE_REVEALJS_"${FILE%.*}".pdf \
  1>> ${LOG_ASCIIDOC_GENERATE} \
  2>> ${LOG_ASCIIDOC_GENERATE_ERROR}

  ERROR=$(cat ${LOG_ASCIIDOC_GENERATE_ERROR})

  # S'il y a eu une erreur lors de la génération, on l'affiche par une popup centrale
  if [ "${ERROR}" != "" ] ; then
  zenity --error \
      --text="${ERROR}"
  fi

fi


if [ "${CONVERT_REVEALJS_TO_PDF}" = "revealjs-sqli" ] ; then
  echo "80"
  echo "# Converting HTML to PDF. It may take a while..."
  docker run --name converthtmltopdf --rm --user ${UID}:${UID} -v "${DIRECTORY}":/slides astefanutti/decktape:1.0.0 \
  SLIDE_REVEALJS_SQLI_"${FILE%.*}".html SLIDE_REVEALJS_SQLI_"${FILE%.*}".pdf \
  1>> ${LOG_ASCIIDOC_GENERATE} \
  2>> ${LOG_ASCIIDOC_GENERATE_ERROR}

  ERROR=$(cat ${LOG_ASCIIDOC_GENERATE_ERROR})

  # S'il y a eu une erreur lors de la génération, on l'affiche par une popup centrale
  if [ "${ERROR}" != "" ] ; then
  zenity --error \
      --text="${ERROR}"
  fi

fi


# Notification par une popup discrète (sortie standard de la génération asciidoc)
#notify-send "`cat $LOG_ASCIIDOC_GENERATE`"
LOG=$(cat ${LOG_ASCIIDOC_GENERATE})


echo "# Job finished. ${LOG}"
echo "100"
) |
zenity --progress \
  --title="Generating..." \
  --auto-kill \
  --pulsate

check_if_cancel_button


### Fin de la barre de progression

#clean_logs
