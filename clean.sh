#!/bin/bash

rm ./test/diagram*.png
rm -rf ./test/PDF_*
rm -rf ./test/HTML_*
rm -rf ./test/SLIDE_*
rm -rf ./test/reveal.js
rm -rf ./test/.asciidoctor
rm -rf ./test/fonts

find ./test/images/* -type d -exec rm -rf {} +
