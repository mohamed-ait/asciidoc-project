#!/bin/bash
# usage :
# docker run --rm -v <dossier_racine>:/documents \
# [ -v $(pwd)/src/templatesqli:/templatesqli \]
#-e "FORMAT=<pdf|pdf-sqli|revealjs|revealjs-sqli|html|html-sqli>" \
#-e "REVEALJSTHEME=<black|white|league|sky|beige|simple|serif|blood|night|moon|solarized>" \
#-e "CSS=<online|offline>" \
#-e "IMAGES=<embedded-in-html|in-local>"
#[-e "THEME=<sqli|rte>]
#[-e "PROFIL=<dev|inte|rec7|prod>"
#[-e "ENV=dev"]
#registry-private.docker.iscbordeaux.lan.bdx.sqli.com/asciidoctor <chemin_fichier_adoc>

# Constantes
VERT="\\033[1;32m"
NORMAL="\\033[0;39m"
ROUGE="\\033[1;31m"

# $1 = paramètre donné au docker run, correspond au nom du fichier asciidoc
# THEME ( optionel ) = le choix du thème pour la génération. Par défaut, celui de SQLI
# PROFIL ( optionnel ) = le choix du profil du destinataire.
# ENV (optionnel) : lorsque définit à "dev", les source utilisées pour la génération des docs proviennent du point de montage.
# Celà permet de modifier les thèmes sans devoir build l'image à chaque modification.

# On récupère le nom du fichier donné en paramètre
FILE=${1}

FILE_NAME=$(basename ${FILE})
DIR_NAME=$(dirname ${FILE})

SOURCE="/opt/templatesqli"

if [[ "${ENV}" == "dev" ]] ; then
    SOURCE="/templatesqli"
fi

SLIDE=${SOURCE}/SLIDE
PDF=${SOURCE}/PDF
HTML5=${SOURCE}/HTML5
PPTX=${SOURCE}/PPTX
DOCX=${SOURCE}/DOCX

# On récupère l'UID de l'owner du fichier asciidoc, car sinon le dossier reveal.js copié
# appartiendra à root et empêchera ses déplacement ou suppression ultérieurs
OWNER=$(ls -ld /documents/"${FILE}" | awk '{print $3}')

# Fonction de génération reveal.js HTML du fichier asciidoc
# TODO factorisation de la génération des slide
generate-revealjs() {

  if [[ "${IMAGES}" == "in-local" ]] ; then
    echo "Local images will be read by the HTML file."
    DATAURI=""
  fi

  if [[ "${IMAGES}" == "embedded-in-html" ]] || [[ "${IMAGES}" == "" ]]; then
    echo "Images will be embedded in the generated HTML file."
    DATAURI="-a data-uri"
  fi

  if [[ "${REVEALJSTHEME}" == "" ]] && [[ "${THEME}" == "" ]] ; then
    REVEALJSTHEME="league"
    echo "No reveal.js theme chosen, it will be by default : ${REVEALJSTHEME}"
  fi

  if [[ "${THEME}" != "" ]] ; then
    REVEALJSTHEME=${THEME}
  fi

  TARGET_FILE_NAME=SLIDE_REVEALJS_"${FILE_NAME%.*}".html
  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    -r asciidoctor-diagram \
    -o "${TARGET_FILE}" \
    -T ${SLIDE}/slim \
    -a backend=revealjs \
    -a revealjs_theme=${REVEALJSTHEME} \
    -a revealjs_slideNumber=true \
    -a revealjs_mouseWheel=true \
    -a source-highlighter=rouge \
    ${DATAURI} \
    ${SOURCE_DIRECTORY}/${FILE} && \
  echo "${TARGET_FILE_NAME} file generated."

  if [[ "${CSS}" == "offline" ]] || [[ "${CSS}" == "" ]]; then
    echo -e "${VERT}Template copied in local.${NORMAL}"
    cp -r ${SLIDE}/reveal.js ${TARGET_DIRECTORY}/${DIR_NAME}
  fi

  if [[ "${CSS}" == "online" ]] ; then
    echo -e "${VERT}External template used.${NORMAL}"
    sed -i "s|reveal\.js\/|https:\/\/asciidoc-templates\.bordeaux\.sqli\.com\/revealjs-template-sqli\/|g" ${TARGET_FILE}
  fi
}

# Fonction de génération reveal.js HTML avec le thème SQLI du fichier asciidoc
generate-revealjs-sqli() {

  if [[ "${IMAGES}" == "in-local" ]] ; then
    echo "Local images will be read by the HTML file."
    DATAURI=""
  fi

  if [[ "${IMAGES}" == "embedded-in-html" ]] || [[ "${IMAGES}" == "" ]]; then
    echo "Images will be embedded in the generated HTML file."
    DATAURI="-a data-uri"
  fi

  TARGET_FILE_NAME=SLIDE_REVEALJS_SQLI_"${FILE_NAME%.*}".html
  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    -r asciidoctor-diagram \
    -r /opt/asciidoctor-chart/lib/asciidoctor-chart.rb \
    -o "${TARGET_FILE}" \
    -T ${SLIDE}/slim \
    -a revealjs_plugins=${SLIDE}/revealjs_plugins \
    -a cssimagesdir=${SLIDE}/reveal.js/css/theme/images \
    -a revealjs_parallaxBackgroundImage=reveal.js/css/theme/images/Bg_light_01.png \
    -a revealjs_parallaxBackgroundSize=cover \
    -a title_bg_dark_01=${SLIDE}/reveal.js/css/theme/images/Bg_dark_01.png \
    -a title_bg_dark_02=${SLIDE}/reveal.js/css/theme/images/Bg_dark_02.png \
    -a title_bg_dark_03=${SLIDE}/reveal.js/css/theme/images/2021_Bg_dark_01.jpg \
    -a title_bg_light_01=${SLIDE}/reveal.js/css/theme/images/Bg_light_01.png \
    -a title_bg_light_02=${SLIDE}/reveal.js/css/theme/images/Bg_light_02.png \
    -a title_bg_light_03=${SLIDE}/reveal.js/css/theme/images/Bg_light_03.png \
    -a title_bg_light_04=${SLIDE}/reveal.js/css/theme/images/Bg_light_04.png \
    -a title_bg_light_05=${SLIDE}/reveal.js/css/theme/images/Bg_light_05-80.png \
    -a title_bg_light_06=${SLIDE}/reveal.js/css/theme/images/Bg_light_06.png \
    -a title_bg_light_07=${SLIDE}/reveal.js/css/theme/images/Bg_light_07.png \
    -a title_bg_light_08=${SLIDE}/reveal.js/css/theme/images/Bg_light_08.png \
    -a title_bg_light_09=${SLIDE}/reveal.js/css/theme/images/Bg_light_09-80.png \
    -a title_bg_light_10=${SLIDE}/reveal.js/css/theme/images/2021_Bg_light_01.png \
    -a title-slide-background-image=${SLIDE}/reveal.js/css/theme/images/2021_Bg_light_01.jpg \
    -a revealjs_theme=sqlislide \
    -a revealjs_slideNumber=true \
    -a revealjs_mouseWheel=true \
    -a source-highlighter=rouge \
    ${DATAURI} \
    ${SOURCE_DIRECTORY}/${FILE} && \
  echo "${TARGET_FILE_NAME} file generated."

  if [[ "${CSS}" == "offline" ]] || [[ "${CSS}" == "" ]]; then
    echo -e "${VERT}Template copied in local.${NORMAL}"
    cp -r ${SLIDE}/reveal.js ${TARGET_DIRECTORY}/${DIR_NAME}
    sed -i "s|${SLIDE}\/reveal\.js|reveal\.js|g" ${TARGET_FILE}
  fi

  if [[ "${CSS}" == "online" ]] ; then
    echo -e "${VERT}External template used.${NORMAL}"
    sed -i "s|reveal\.js\/|https://asciidoc-templates\.bordeaux\.sqli\.com\/revealjs-template-sqli\/|g" ${TARGET_FILE}
  fi
  # Remplacement d'URL HTTP en HTTPS (quelques ressources externes sont par défaut en HTTP, créant des erreurs "Mixed Content HTTP/HTTPS")
  sed -i "s|http://cdn|https://cdn|g" ${TARGET_FILE}
}

# Fonction de génération HTML du fichier asciidoc
generate-html() {
  HTML_THEME=""
  HTML_PROFIL=""

  if [[ "${IMAGES}" == "in-local" ]] ; then
    echo "Local images will be read by the HTML file."
    DATAURI=""
  fi

  if [[ "${IMAGES}" == "embedded-in-html" ]] || [[ "${IMAGES}" == "" ]]; then
    echo "Images will be embedded in the generated HTML file."
    DATAURI="-a data-uri"
  fi

  # Paramétrage du profil
  if [[ "${PROFIL}" != "" ]] ; then
    HTML_PROFIL="-a profil="${PROFIL}
  fi

  # Paramétrage du thème
  if [[ "${THEME}" != "" ]] ; then
    HTML_THEME="-a stylesheet=${HTML5}/stylesheets/${THEME}template.css"

    # On ajoute le nom du thème dans le fichier de sortie, en lettres capitales
    TARGET_FILE_NAME=HTML_${THEME}_"${FILE_NAME%.*}".html
  else
    # Nom standard du fichier de sortie, sans thème précisé
    TARGET_FILE_NAME=HTML_"${FILE_NAME%.*}".html
  fi

  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    ${HTML_THEME} \
    ${HTML_PROFIL} \
    ${DATAURI} \
    -r asciidoctor-diagram \
    -r /opt/asciidoctor-chart/lib/asciidoctor-chart.rb \
    -a source-highlighter=rouge \
    -o "${TARGET_FILE}" \
    -a cssimagesdir=${HTML5}/images/ \
    ${SOURCE_DIRECTORY}/${FILE} && \
  echo "${TARGET_FILE_NAME} file generated."

  if [[ "${THEME}" != "" ]] ; then

    if [[ "${CSS}" == "offline" ]] || [[ "${CSS}" == "" ]]; then
      echo -e "${VERT}Template copied in local.${NORMAL}"
      cp -r ${HTML5}/images ${TARGET_DIRECTORY}/${DIR_NAME}/
      cp -r ${HTML5}/fonts ${TARGET_DIRECTORY}/${DIR_NAME}/
    fi

    if [[ "${CSS}" == "online" ]] ; then
      echo -e "${VERT}External template used.${NORMAL}"
      sed -i 's/url(images/url\(https:\/\/asciidoc-templates\.bordeaux\.sqli\.com\/html-template-sqli\/images/g' ${TARGET_FILE}
      sed -i 's/url(fonts/url\(https:\/\/asciidoc-templates\.bordeaux\.sqli\.com\/html-template-sqli\/fonts/g' ${TARGET_FILE}
    fi
  fi
  # Remplacement d'URL HTTP en HTTPS (quelques ressources externes sont par défaut en HTTP, créant des erreurs "Mixed Content HTTP/HTTPS")
  sed -i "s|http://cdn|https://cdn|g" ${TARGET_FILE}
}

# Fonction de génération HTML avec le template SQLI du fichier asciidoc
generate-html-sqli() {
  if [[ "${IMAGES}" == "in-local" ]] ; then
    echo "Local images will be read by the HTML file."
    DATAURI=""
  fi

  if [[ "${IMAGES}" == "embedded-in-html" ]] || [[ "${IMAGES}" == "" ]]; then
    echo "Images are embedded in the generated HTML file."
    DATAURI="-a data-uri"
  fi

  TARGET_FILE_NAME=HTML_SQLI_"${FILE_NAME%.*}".html
  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    -a stylesheet=${HTML5}/stylesheets/sqlitemplate.css \
    ${DATAURI} \
    -r asciidoctor-diagram \
    -r /opt/asciidoctor-chart/lib/asciidoctor-chart.rb \
    -o "${TARGET_FILE}" \
    -a cssimagesdir=${HTML5}/images/ \
    -a source-highlighter=rouge \
    ${SOURCE_DIRECTORY}/${FILE} && \
    echo "${TARGET_FILE_NAME} file generated."

  if [[ "${CSS}" == "offline" ]] || [[ "${CSS}" == "" ]]; then
    echo -e "${VERT}Template copied in local.${NORMAL}"
    cp -r ${HTML5}/images ${TARGET_DIRECTORY}/${DIR_NAME}/
    cp -r ${HTML5}/fonts ${TARGET_DIRECTORY}/${DIR_NAME}/
  fi

  if [[ "${CSS}" == "online" ]] ; then
    echo -e "${VERT}External template used.${NORMAL}"
    sed -i 's/url(images/url\(https:\/\/asciidoc-templates\.bordeaux\.sqli\.com\/html-template-sqli\/images/g' ${TARGET_FILE}
    sed -i 's/url(fonts/url\(https:\/\/asciidoc-templates\.bordeaux\.sqli\.com\/html-template-sqli\/fonts/g' ${TARGET_FILE}
 fi
  # Remplacement d'URL HTTP en HTTPS (quelques ressources externes sont par défaut en HTTP, créant des erreurs "Mixed Content HTTP/HTTPS")
  sed -i "s|http://cdn|https://cdn|g" ${TARGET_FILE}
}

# Fonction de génération PDF du fichier asciidoc
generate-pdf() {
  PDF_STYLE_DIR=""
  PDF_STYLE=""
  PDF_FONTS=""
  PDF_PROFIL=""
  TARGET_FILE_NAME=PDF_"${FILE_NAME%.*}".pdf

  # Paramétrage du thème
  if [[ "${THEME}" != "" ]] ; then
    TARGET_FILE_NAME=PDF_${THEME}_"${FILE_NAME%.*}".pdf
    PDF_STYLE="-a pdf-style=${THEME}"
    PDF_STYLE_DIR="-a pdf-stylesdir=${PDF}/data/themes"
    PDF_FONTS="-a pdf-fontsdir=${PDF}/data/fonts"
  fi

  # Paramétrage du profil
  if [[ "${PROFIL}" != "" ]] ; then
    PDF_PROFIL="-a profil="${PROFIL}
  fi

  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    -r asciidoctor-pdf \
    -b pdf \
    -r asciidoctor-diagram \
    ${PDF_STYLE_DIR} \
    ${PDF_STYLE} \
    ${PDF_FONTS} \
    -a allow-uri-read \
    -a icons=font \
    -a source-highlighter=rouge \
    -a chapter-label \
    ${PDF_PROFIL} \
    -o "${TARGET_FILE}" \
    ${SOURCE_DIRECTORY}/${FILE} && \
  echo "${TARGET_FILE_NAME} file generated."
}

# Fonction de génération PDF avec le thème SQLI du fichier asciidoc
# DEPRECATED, conservée pour rétro compatibilité
generate-pdf-sqli() {
  echo -e "${ROUGE}""DEPRECATED : utiliser l'option FORMAT=pdf avec l'option THEME=sqli.""${NORMAL}"

  TARGET_FILE_NAME=PDF_SQLI_"${FILE_NAME%.*}".pdf
  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    -r asciidoctor-pdf \
    -b pdf \
    -r asciidoctor-diagram \
    -a pdf-stylesdir=${PDF}/data/themes \
    -a pdf-style=sqli \
    -a pdf-fontsdir=${PDF}/data/fonts \
    -a icons=font \
    -a source-highlighter=rouge \
    -a allow-uri-read \
    -a chapter-label \
    -o "${TARGET_FILE}" \
    ${SOURCE_DIRECTORY}/$FILE && \
  echo "${TARGET_FILE_NAME} file generated."
}

# Fonction de génération présentation Powerpoint du fichier asciidoc
generate-pptx() {
  if [[ "${THEME}" == "" ]] ; then
    THEME="sqli"
    echo "No theme chosen, it will be by default : ${THEME}"
  fi

  TARGET_FILE_NAME=PPTX_SQLI_"${FILE_NAME%.*}".pptx
  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    -b docbook \
    -o - ${SOURCE_DIRECTORY}/${FILE} | \
    /usr/bin/pandoc \
    --reference-doc "${PPTX}/${THEME}-theme.pptx" \
    -f docbook \
    -t pptx \
    -o "${TARGET_FILE}" \
    && \
  echo "${TARGET_FILE_NAME} file generated."
}

# Fonction de génération présentation word du fichier asciidoc
generate-docx() {
  if [[ "${THEME}" == "" ]] ; then
    THEME="sqli"
    echo "No theme chosen, it will be by default : ${THEME}"
  fi

  TARGET_FILE_NAME=DOCX_SQLI_"${FILE_NAME%.*}".docx
  TARGET_FILE=${TARGET_DIRECTORY}/${DIR_NAME}/${TARGET_FILE_NAME}

  /usr/bin/asciidoctor \
    -b docbook \
    -o - ${SOURCE_DIRECTORY}/${FILE} | \
    /usr/bin/pandoc \
    --reference-doc "${DOCX}/${THEME}-theme.docx" \
    -f docbook \
    -t docx \
    -o "${TARGET_FILE}" \
    && \
  echo "${TARGET_FILE_NAME} file generated."
}

case "${FORMAT}" in
  "pdf")
    generate-pdf
    ;;
  "pdf-sqli")
    generate-pdf-sqli
    ;;
  "revealjs")
    generate-revealjs
    ;;
  "revealjs-sqli")
    generate-revealjs-sqli
    ;;
  "pptx")
    generate-pptx
    ;;
  "docx")
    generate-docx
    ;;
  "html")
    generate-html
    ;;
  "html-sqli")
    generate-html-sqli
    ;;
  *)
    # S'il n'y a pas de format précisé, on utilise par défaut l'HTML avec le thème SQLI
    generate-html-sqli
    ;;
esac

# On modifie le owner de tous les documents du répertoire de travail
chown -R ${OWNER}:${OWNER} ${TARGET_DIRECTORY}
